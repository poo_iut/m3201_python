import pickle as p
import matplotlib.pyplot as plt
import numpy as np

def taille(liste):
    return len(liste)

def moyenne(liste):
    return float(sum(liste)) / float(taille(liste))

def mediane(liste):
    milieu = float(taille(liste)/ 2);
    if int(milieu) == milieu :
        milieu = int(milieu)
        return float(liste[milieu] + liste[milieu+1]) / 2
    else:
        milieu = int(milieu)
        return liste[milieu]

def covariance(listeA, listeB):
    sommeXY = sum(map(lambda x,y:x*y,listeA,listeB))
    moyenneA = moyenne(listeA)
    moyenneB = moyenne(listeB)
    return (1/len(listeA) * sommeXY) - (moyenneA * moyenneB)

def regregLin(listeA, listeB):
    cov = covariance(listeA, listeB)
    ecartTypeA = ecartType(listeA)
    ecartTypeB = ecartType(listeB)
    return cov / (ecartTypeA * ecartTypeB)

def ecartType(liste):
    return np.sqrt(variance(liste))

def variance(liste):
    m = moyenne(liste)
    nb = taille(liste)
    somme = sum(map(lambda x: 1 * ((x - m)**2), liste))
    return (1/float(nb) * somme)

# On charge le pickle, read(fichier, mode_ouverture)
obj = p.load(open("data5.txt", "rb"), encoding="bytes")

l = obj[:, 0]
w = obj[:, 1]

print("Il y a un lot de %s données" % len(obj))
print("Moyenne de l = %s, Moyenne de w = %s " % (moyenne(l), moyenne(w)))
print("Variance de l = %s, Variance de w = %s " % (variance(l), variance(w)))
print("Covariance de l et w = %s " % covariance(l, w))

r = regregLin(l, w)

print("Coef de regression linéaire de l et w = %s " % r)

def funcD(x, a, b):
    return [x, a * x + b]



a = covariance(l, w) / ecartType(l)**2
b = moyenne(w) - a*(moyenne(l))

# Titre
plt.title('Nuage de points')

#print("%s || %s "%(premierPoint, dernierPoint))
# Label des axes
plt.xlabel('Longueur du Nez (l)')
plt.ylabel('Largeur du Nez (w)')

minX = int(min(l))
maxX = int(max(l))

x = np.arange(minX, maxX)

# Droite + Nuage de point
plt.subplot(223)
plt.plot(x, a*x + b)
# Nuage de point
plt.scatter(l, w)

# Histogramme L
plt.subplot(222)
plt.hist(l, range = (minX, maxX),
            bins = 15,
            color = 'red')

# Histogramme W
plt.subplot(221)
plt.hist(w, range = (min(w), max(w)),
            bins = 15,
            color = 'orange')



# Affichage
plt.show()
