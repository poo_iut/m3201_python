import pickle as p
import matplotlib.pyplot as plt
import numpy as np

d = np.array([[1, 963],
              [2, 2371],
              [3, 752],
              [4, 24]])

n = ['Steack-purée', 'Coquillettes-jambon', 'Cassoulet', 'Epinards-saumon']


plt.bar(n, d[:, 1])
plt.show()
