import pickle as p
import matplotlib.pyplot as plt
import numpy as np

# On charge le pickle, read(fichier, mode_ouverture)
T = p.load(open("sujet1.txt", "rb"), encoding="bytes")
T.sort(axis=0)
x = T[:, 0]
y = T[:, 1]

def taille(liste):
    return len(liste)

def moyenne(x):
    return sum(x) / len(x);

def variance(liste):
    m = moyenne(liste)
    nb = taille(liste)
    somme = sum(map(lambda x: 1 * ((x - m)**2), liste))
    return (1/float(nb) * somme)

def regregLin(listeA, listeB):
    cov = covariance(listeA, listeB)
    ecartTypeA = ecartType(listeA)
    ecartTypeB = ecartType(listeB)
    return cov / (ecartTypeA * ecartTypeB)

def ecartType(liste):
    return np.sqrt(variance(liste))

def covariance(listeA, listeB):
    sommeXY = sum(map(lambda x,y:x*y,listeA,listeB))
    moyenneA = moyenne(listeA)
    moyenneB = moyenne(listeB)
    return (1/len(listeA) * sommeXY) - (moyenneA * moyenneB)

#Refaire les calculs de médianes et autres en repensant au fait qu'il y ai valeur + effectif

print("Effectif de X,Y : %s" % taille(T))
print("Effectif de X : %s" % taille(x))
print("Effectif de Y : %s" % taille(y))

print("Moyenne X: %s" % moyenne(x))
print("Variance X : %s" % variance(x))

print("Moyenne Y: %s" % moyenne(y))
print("Variance Y : %s" % variance(y))

print("Covariance de X,Y : %s" % covariance(x, y))
print("Coef de regression linéaire X,Y : %s" % regregLin(x, y))

# Titre
plt.title('Nuage de points')

a = covariance(x, y) / ecartType(x)**2
b = moyenne(y) - a*(moyenne(x))

plt.figure(dpi=150)
# Label des axes
plt.xlabel('Longueur sépales Iris (x)')
plt.ylabel('Largeur sépales Iris (y)')

minX = int(min(x))
maxX = int(max(x))

x_ = np.arange(minX, maxX)

# Droite + Nuage de point
plt.plot(x_, a*x_ + b)
# Nuage de point
plt.scatter(x, y)

plt.show()
