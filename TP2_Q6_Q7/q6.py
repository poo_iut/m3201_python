import pickle as p
import matplotlib.pyplot as plt
import numpy as np

# On charge le pickle, read(fichier, mode_ouverture)
obj = p.load(open("data5.txt", "rb"), encoding="bytes")

xData = list(map(lambda x: x[0], obj))
yData = list(map(lambda x: x[1], obj))

print("Il y a un lot de %s données" % len(obj))


# Nuage de point
plt.scatter(xData, yData)

# Titre
plt.title('Nuage de points')
# Label des axes
plt.xlabel('Longueur du Nez')
plt.ylabel('Largeur du Nez')
# Affichage
plt.show()
