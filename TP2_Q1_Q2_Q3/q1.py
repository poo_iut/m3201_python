import pickle as p
import matplotlib.pyplot as pp
import numpy as np

# On charge le pickle, read(fichier, mode_ouverture)
obj = p.load(open("data2.txt", "rb"), encoding="bytes")
obj = obj[:, 0]

obj = obj[60:]

def taille(liste):
    return len(liste)

def moyenne(liste):
    return float(sum(liste)) / float(taille(liste))

def mediane(liste):
    milieu = float(taille(liste)/ 2);
    if int(milieu) == milieu :
        milieu = int(milieu)
        return float(liste[milieu] + liste[milieu+1]) / 2
    else:
        milieu = int(milieu)
        return liste[milieu]

def ecartType(liste):
    m = moyenne(liste)
    nb = taille(liste)
    somme = sum(map(lambda x: 1 * ((x - m)**2), liste))
    return np.sqrt(1/float(nb) * somme)

# Nombre de commune
print("Nb de commune: "+str(taille(obj)))
# Min / Max
print("Min: %s, Max: %s" % (min(obj), max(obj)))

# Moyenne
print("Moyenne : %s, Ecart Type: %s" % (moyenne(obj), ecartType(obj)))

#Mediane VS Moyenne
print("Moyenne : %s, Médiane : %s" % (moyenne(obj), mediane(obj)))


# Histogramme logarithmique

pp.figure(dpi=200)
pp.hist(obj, range = (min(obj), max(obj)),
            bins = 200,
            color = 'red',
            log = True)
            
pp.xlabel('habitant')
pp.ylabel('commune')
pp.title('Commune de France')
pp.show()
