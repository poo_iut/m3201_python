import pickle as p
import matplotlib.pyplot as pp
import numpy as np
from random import gauss

# On charge le pickle, read(fichier, mode_ouverture)
obj = p.load(open("data4.txt", "rb"), encoding="bytes")
obj.sort()

def taille(liste):
    return len(liste)

def moyenne(liste):
    return float(sum(liste)) / float(taille(liste))

def mediane(liste):
    milieu = float(taille(liste)/ 2);
    if int(milieu) == milieu :
        milieu = int(milieu)
        return float(liste[milieu] + liste[milieu+1]) / 2
    else:
        milieu = int(milieu)
        return liste[milieu]

def ecartType(liste):
    m = moyenne(liste)
    nb = taille(liste)
    somme = sum(map(lambda x: 1 * ((x - m)**2), liste))
    return np.sqrt(1/float(nb) * somme)

print("Taille: %s, Moyenne: %s, Mediane: %s, EcartType: %s" % (taille(obj), moyenne(obj), mediane(obj), ecartType(obj)))


x = [gauss(moyenne(obj),ecartType(obj)) for i in range(500)]

num_bins = 2
n, bins, patches = pp.hist(x, num_bins, density=5, facecolor='green', alpha=0.5)

pp.show()
